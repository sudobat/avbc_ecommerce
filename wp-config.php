<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ecommerce');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Pk+MALB^R1b_}62Ow?lac`xc`=IV{/eO@J[iSK+M=W]5}-}o3utA|K-,8+a8 S:w');
define('SECURE_AUTH_KEY',  ';&=OAottz>E 36G|Q L<D-g8si+qk4pGn@#AWBf$BLS(F7@yJ?6mf7my2A:B5Osf');
define('LOGGED_IN_KEY',    ']MjaO1.BK*8Ic#n[wk5e#?lx~/:mxJes2&FyRw|>Tb-KJnf*%&[ b%Y+(-M|O;j.');
define('NONCE_KEY',        'IEm+bTo01L873%.S`j4-$k#;6;C|Xsz{2&ti{8|-`I+@Wn%vjvltJ pZHwmnTe+X');
define('AUTH_SALT',        '0w//sZu-R$NK/R%B1|i>s<)-6;7-+GXBknC:p^|~c6>lN##5GR6k^kw:(5>qtAg(');
define('SECURE_AUTH_SALT', ']WH*C#DtWoZyfTV>c_SMU*7~XIIc?3gvf0`uYSC1(g4E/{Yba41I{+m3,RNz3v~>');
define('LOGGED_IN_SALT',   '[-|!M+^ gy7*Y2Ix#Zgj|L!jU+__OmH;tZjQDM7v.VeiDMbas]_*Dm-*+t_dz&^h');
define('NONCE_SALT',       '1Gtv;Ylgs$uU}^,F[-(_*V2Jr-3-U69AwW)r8{V`b+X*[I/N.{5PAy1c-SQnh!7K');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ecommerce_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
